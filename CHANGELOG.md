## Version 2.1.1

1. 在DevEco Studio: NEXT Beta1-5.0.3.806, SDK:API12 Release(5.0.0.66)上验证通过
2. 修复EventBus注册的事件无法解注册的问题
3. 修复postValue方法forEach遍历移除监听时异常现象
4. 新增取消App之间的消息订阅接口
   LiveEventBus.get("key").unsubscribe
5. 修复不兼容API9问题

## Version 2.1.1-rc.3

1. 修复EventBus注册的事件无法解注册的问题

## Version 2.1.1-rc.2

1. 修复postValue方法forEach遍历移除监听时异常现象

## Version 2.1.1-rc.1

1. 新增取消App之间的消息订阅接口
   LiveEventBus.get("key").unsubscribe

## Version 2.1.1-rc.0

1. 修复不兼容API9问题

## Version 2.1.0

1. 适配DevEco Studio: 4.0 (4.0.3.512), SDK: API10 (4.0.10.9)
2. ArkTs语法适配
3. 接口使用方式变更：GlobalContext替代globalThis
4. 新增Observer导出声明
5. 处理模块导出类的路径大小问题

## Version 2.0.0

1. 适配DevEco Studio: 3.1 Beta2(3.1.0.400), SDK: API9 Release(3.2.11.9)
2. 包管理工具由npm切换成ohpm

## v1.1.0
1. 名称由LiveEventBus-ETS修改LiveEventBus。
2. 旧的包@ohos/LiveEventBus-ETS已不维护，请使用新包@ohos/LiveEventBus

## v1.0.1

1. api8升级到api9

## v1.0.0

1. 支持采用观察者模式实现事件总线
2. 支持不定义消息直接发送和先定义消息再发送
3. 支持进程内消息发送
4. 支持App内发送消息，跨进程使用
5. 支持App之间发送消息
6. 支持延迟发送消息
7. 支持Sticky粘性消息
8. 支持生命周期感知能力